const mongoose = require('mongoose')
const config = require('./config')

mongoose.connect(config.database_url, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true , useFindAndModify:false}, (err, res) => {
    if (err) throw err;
    console.log('Base de datos online', res.connections[0].host);
});

module.exports=mongoose