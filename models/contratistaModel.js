const mongoose = require('mongoose')

let Schema = mongoose.Schema

let contratistaSchema = new Schema({
    rut:{
        type:String,
        required: [true, 'El rut es obligatorio'],
        unique:true
    },
    nombre:{
        type:String,
        required:[true, 'El nombre es obligatorio']
    },
    estado:{
        type:Boolean,
        default: true
    },
    representante:{
        type: Object,
        required:true
    },
    especialidad:{
        type: Object,
        required:true
    }
})

module.exports = mongoose.model('Contratistas',contratistaSchema)