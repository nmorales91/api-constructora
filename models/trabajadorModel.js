const mongoose = require('mongoose')

let Schema = mongoose.Schema

let trabajadorSchema = new Schema({
    rut:{
        type:String,
        required: [true, 'El rut es obligatorio'],
        unique:true
    },
    nombre:{
        type:String,
        required:[true, 'El nombre es obligatorio']
    },
    apellido_paterno:{
        type:String,
        required:[true, 'El apellido es obligatorio']
    },
    apellido_materno:{
        type:String,
    },
    fecha_nacimiento:{
        type:Date
    },
    direccion:{
        type:String
    },
    ciudad:{
        type:String
    },
    comuna:{
        type:String
    },
    region:{
        type:String
    },
    vehiculo:{
        type:String
    },
    nacionalidad:{
        type:String
    },
    estado:{
        type:Boolean,
        default: true
    },
    contratista:{
        type: Object,
        required:true
    },
    constructora:{
        type: Object,
        required:true
    },
    codigo:{
        type:String,
        required: true
    },
    fecha_registro:{
        type:Date,
        default:Date.now
    },
})

module.exports = mongoose.model('Trabajador',trabajadorSchema)