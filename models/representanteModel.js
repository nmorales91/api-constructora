const mongoose = require('mongoose')

let Schema = mongoose.Schema

let representanteSchema = new Schema({
    rut:{
        type:String,
        required: [true, 'El rut es obligatorio'],
        unique:true
    },
    nombre:{
        type:String,
        required:[true, 'El nombre es obligatorio']
    },
    direccion:{
        type:String,
    },
    ciudad:{
        type:String,
    },
    comuna:{
        type:String
    },
    region:{
        type:String,
    },
    telefono:{
        type:String,
    },
    correo:{
        type:String,
        unique:true,
        required:[true, 'El correo es requerido']
    }
})

module.exports = mongoose.model('Representantes',representanteSchema)