const mongoose = require('mongoose')

let Schema = mongoose.Schema

let asistenciaSchema = new Schema({
    id_usuario:{
        type: mongoose.Types.ObjectId,
        required: [true, 'El id es requerido'],
    },
    fecha:{
        type:Date,
        required: [true, 'La fecha es requerida'],
    },
    horas:{
        type:Array,
        required: [true, 'La hora es requerida']
    },
    tipo_usuario:{
        type: Number,
        required: [true, 'El tipo de usuario es requerido']
    }
})

module.exports = mongoose.model('Asistencia',asistenciaSchema,'asistencia')