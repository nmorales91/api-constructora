const mongoose = require('mongoose')

let Schema = mongoose.Schema

let constructoraSchema = new Schema({
    rut:{
        type:String,
        required: true,
        unique:true
    },
    nombre:{
        type:String,
        required:true
    },
    giro:{
        type:String,
    },
    direccion:{
        type:String,
    },
    ciudad:{
        type:String,
    },
    comuna:{
        type:String
    },
    region:{
        type:String,
    },
    telefono:{
        type:String,
    },
    correo:{
        type:String,
        unique:true
    }
})

module.exports = mongoose.model('Constructoras',constructoraSchema)