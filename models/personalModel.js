const mongoose = require('mongoose')

let Schema = mongoose.Schema

let personalSchema = new Schema({
    rut:{
        type:String,
        required: [true, 'El rut es obligatorio'],
        unique:true
    },
    nombre:{
        type:String,
        required:[true, 'El nombre es obligatorio']
    },
    apellido_paterno:{
        type:String,
        required:[true, 'El apellido es obligatorio']
    },
    apellido_materno:{
        type:String,
    },
    fecha_nacimiento:{
        type:Date
    },
    direccion:{
        type:String
    },
    ciudad:{
        type:String
    },
    fecha_ingreso:{
        type:Date,
        required:[true, 'La fecha de ingreso es obligatoria']
    },
    estado_civil:{
        type:String
    },
    cargo:{
        type:String
    },
    sueldo:{
        type:Number
    },
    afp:{
        type:String
    },
    salud:{
        type:String
    },
    duracion_contrato:{
        type:Date
    },
    telefono:{
        type:String
    },
    medio_pago:{
        type:String
    },
    escolaridad:{
        type:String
    },
    nacionalidad:{
        type:String
    },
    estado:{
        type:Boolean,
        default: true
    },
    constructora:{
        type: Object,
        required:true
    },
    codigo:{
        type:String,
        required: true
    },
    fecha_registro:{
        type:Date,
        default:Date.now
    },
})

module.exports = mongoose.model('Personal',personalSchema, 'Personal')