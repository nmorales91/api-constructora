const mongoose = require('mongoose')

let Schema = mongoose.Schema

let rolesSchema = new Schema({
    nombre:{
        type:String,
        required: [true, 'El nombre es requerido'],
        unique:true
    },
})

module.exports = mongoose.model('Roles',rolesSchema)