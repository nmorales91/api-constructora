const mongoose = require('mongoose')
const Roles = require('../models/rolesModel')

const resultRoles = async (nombre) => {
    return await Roles.findOne({nombre})
}

let Schema = mongoose.Schema

let usuarioSchema = new Schema({
    correo:{
        type:String,
        required: [true,'El correo es necesario'],
        unique:true
    },
    clave:{
        type:String,
        required:[true,'La clave es necesaria']
    },
    rol:{
        type:String,
        required:true,
        validate: (nombre) => (resultRoles(nombre.toUpperCase()))
    },
    nombre:{
        type:String,
        required:true
    },
    apellido_paterno:{
        type:String,
        required:true
    },
    apellido_materno:{
        type:String
    },
    estado:{
        type:Boolean,
        default:true
    },
    fecha_registro:{
        type:Date,
        default: Date.now
    },
    ultimo_ingreso:{
        type:Date
    }
})

usuarioSchema.methods.toJSON = function(){
    let user = this;
    let userObject = user.toObject();
    delete userObject.clave;

    return userObject
}

module.exports = mongoose.model('Usuarios',usuarioSchema)
