const Especialidad = require('../models/especialidadModel')
const mongoose = require('mongoose')

exports.getEspecialidades = async () => {
    try {
        const response = await Especialidad.find();
        return response
    } catch (e) {
        throw Error('Error al consultar especialidades')
    }
}

exports.saveEspecialidad = async nombre => {
    const especialidad = await Especialidad.findOne({nombre})
    if(especialidad){
        throw Error('Especialidad ya existe')
    }
    const response = await Especialidad.create({nombre})
    return response
}

exports.findEspecialidad = async id => {
    try{
        const especialidad = await Especialidad.findById(id)
        if(!especialidad){
            return ({data: null,  message: 'Especialidad no encontrada' });
        }
        return ({data: especialidad,  message: 'Especialidad encontrada' });
    } catch (e) {
        if(e instanceof mongoose.Error.CastError){
            return ({data: null,  message: 'Id no válido' });
        }
        throw Error('Error al buscar especialidad')
    }
}
