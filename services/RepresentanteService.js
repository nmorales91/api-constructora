const Representante = require('../models/representanteModel')
const mongoose = require('mongoose')

exports.getRepresentantes = async () => {
    try {
        const response = await Representante.find();
        return response
    } catch (e) {
        throw Error('Error al consultar representantes')
    }
}

exports.saveRepresentantes = async (rut, nombre, direccion,ciudad, comuna, region, telefono, correo) => {
    const representante = await Representante.findOne({rut})
    if(representante){
        throw Error('Representante ya existe')
    }
    const response = await Representante.create({rut, nombre, direccion,ciudad, comuna, region, telefono, correo})
    return response
}

exports.findRepresentante = async id => {
    try{
        const representante = await Representante.findById(id)
        if(!representante){
            return ({data: null,  message: 'Representante no encontrado' });
        }
        return ({data: representante,  message: 'Representante encontrado' });
    } catch (e) {
        if(e instanceof mongoose.Error.CastError){
            return ({data: null,  message: 'Id no válido' });
        }
        throw Error('Error al buscar representante')
    }
}
