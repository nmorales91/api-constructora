const Contratistas = require('../models/contratistaModel')
const Especialidades = require('../models/especialidadModel')
const Representantes = require('../models/representanteModel')
const mongoose = require('mongoose')

exports.getAllContratistas = async (state=false) => {
    try {
        let contratistas = []
        if(state){
            contratistas = await Contratistas.find({estado:state});
        }else{
            contratistas = await Contratistas.find();
        }
        return contratistas
    } catch (e) {
        throw Error('Error al consultar Contratistas')
    }
}

exports.saveContratista = async (rut, nombre, especialidad, representante) => {
    const contratista = await Contratistas.findOne({rut})
    if(contratista){
        throw Error('Contratista ya existe')
    }

    const response = await Contratistas.create({
        rut, nombre, especialidad, representante
    })
    return response
}

exports.findContratista = async id => {
    try{
        const contratista = await Contratistas.findOne({_id:id, estado:true})
        if(!contratista){
            return ({data: null,  message: 'Contratista no encontrado' });
        }
        return ({data: contratista,  message: 'Contratista encontrado' });
    } catch (e) {
        if(e instanceof mongoose.Error.CastError){
            return ({data: null,  message: 'Id no válido' });
        }
        throw Error('Error al buscar contratista')
    }
}

exports.updateContratista = async (id, contratista, rut, nombre, id_especialidad, id_representante) => {
    try {
        let especialidad = contratista.especialidad
        if (id_especialidad && id_especialidad !== contratista.especialidad._id) {
            especialidad = await Especialidades.findById(id_especialidad)
        }
        let representante = contratista.representante
        if (id_representante && id_representante !== contratista.representante._id) {
            representante = await Representantes.findById(id_representante)
        }
        const newContratista = {
            nombre: nombre ? nombre : contratista.nombre,
            rut : rut ? rut : contratista.rut,
            especialidad : id_especialidad ? especialidad : contratista.especialidad,
            representante : id_representante ? representante : contratista.representante
        }

        const response = await Contratistas.findByIdAndUpdate(id, { ...newContratista } ,{new: true, runValidators:true})
        return ({data: response });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}

exports.deleteContratista = async id =>{
    try {
        const response = await Contratistas.findByIdAndUpdate(id, { estado:false } ,{new: true})
        return ({data: response });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}
