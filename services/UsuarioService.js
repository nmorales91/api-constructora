const Usuarios = require('../models/usuarioModel')
const bcrypt = require("bcryptjs");
const mongoose = require('mongoose')

exports.getAllUsers = async (state=false) => {
    try {
        let usuarios = []
        if(state){
            usuarios = await Usuarios.find({estado:state});
        }else{
            usuarios = await Usuarios.find();
        }
        return usuarios
    } catch (e) {
        throw Error('Error al consultar usuarios')
    }
}

exports.saveUser = async (correo,clave,rol,nombre, apellido_paterno, apellido_materno) => {
    const usuario = await Usuarios.findOne({correo})
    if(usuario){
        return ({data: null,  message: `Usuario ${correo} ya existe` });
    }

    const response = await Usuarios.create({
        correo,
        rol: rol.toUpperCase(),
        clave: bcrypt.hashSync(clave, 10),
        apellido_materno,
        apellido_paterno,
        nombre
    })
    return ({data: response,  message: `Usuario agregado con éxito` });
}

exports.findUser = async id => {
    try{
        const usuario = await Usuarios.findById(id)
        if(!usuario){
            return ({data: null,  message: 'Usuario no encontrado' });
        }
        return ({data: usuario,  message: 'Usuario encontrado' });
    } catch (e) {
        if(e instanceof mongoose.Error.CastError){
            return ({data: null,  message: 'Id no válido' });
        }
        throw Error('Error al buscar usuario')
    }
}

exports.updateUser = async (id,usuario, nombre, apellido_paterno, apellido_materno, correo, clave, rol) => {
    try {
        const newUser = {
            nombre: nombre ? nombre : usuario.nombre,
            apellido_paterno : apellido_paterno ? apellido_paterno : usuario.apellido_paterno,
            apellido_materno: apellido_materno ? apellido_materno : usuario.apellido_materno,
            correo : correo ? correo : usuario.correo,
            clave: clave ? bcrypt.hashSync(clave, 10): usuario.clave,
            rol : rol ? rol : usuario.rol,
        }
        const response = await Usuarios.findByIdAndUpdate(id, { ...newUser } ,{new: true, runValidators:true})
        return ({data: response });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}

exports.deleteUser = async id =>{
    try {
        const response = await Usuarios.findByIdAndUpdate(id, { estado:false } ,{new: true})
        return ({data: response });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}
