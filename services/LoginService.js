const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const moment = require('moment')

const Usuarios = require("../models/usuarioModel");
const config = require("../config/config")


exports.loginUser = async (correo, clave) => {
    const usuario = await Usuarios.findOne({correo})
    if(!usuario){
        return ({data: null,  message: 'Correo no válido' });
    }

    if(!usuario.estado){
        return ({data: null,  message: 'Usuario inactivo' });
    }

    if(!bcrypt.compareSync(clave,usuario.clave)){
        return ({data: null,  message: 'Clave no válida' });
    }

    let userUpdate = await Usuarios.findByIdAndUpdate(usuario._id,{ultimo_ingreso:moment()}, {new:true})

    let token = jwt.sign({
        usuariobd:userUpdate
    },config.secret,{expiresIn:config.expires})

    let response = {
        usuariobd:userUpdate,
        token
    }

    return ({ data: response });

}
