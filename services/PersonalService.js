const Personal = require('../models/personalModel')
const Constructoras = require('../models/constructoraModel')
const mongoose = require('mongoose')
const { nanoid } = require('nanoid')

exports.getAllPersonal = async (state=false) => {
    try {
        let personal = []
        if(state){
            personal = await Personal.find({estado:state});
        }else{
            personal = await Personal.find();
        }
        return personal
    } catch (e) {
        throw Error('Error al consultar Personal')
    }
}

exports.savePersonal = async (personal, constructora) => {
    const personalFind = await Personal.findOne({rut: personal.rut})
    if(personalFind){
        throw Error('Personal ya existe')
    }
    const personalSave = {...personal, codigo: "PER-"+personal.rut + nanoid()}
    const response = await Personal.create({...personalSave, constructora})
    return response
}

exports.findPersonal = async id => {
    try{
        const personal = await Personal.findById(id)
        if(!personal){
            return ({data: null,  message: 'Personal no encontrado' });
        }
        return ({data: personal,  message: 'Personal encontrado' });
    } catch (e) {
        if(e instanceof mongoose.Error.CastError){
            return ({data: null,  message: 'Id no válido' });
        }
        throw Error('Error al buscar personal')
    }
}

exports.updatePersonal = async (id, personalDB, personalRequest) => {
    try {
        let constructora = personalDB.constructora
        if (personalRequest.id_constructora && personalRequest.id_constructora !== constructora._id) {
            constructora = await Constructoras.findById(personalRequest.id_constructora)
        }
        let newPersonal = {...personalDB._doc}

        for (const property in personalRequest) {
            if(property !== 'id_constructora' && personalDB[property] !== personalRequest[property]){
                newPersonal[property] = personalRequest[property]
            }
        }

        newPersonal.constructora = constructora

        const response = await Personal.findByIdAndUpdate(id, { ...newPersonal } ,{new: true, runValidators:true})
        return ({data: response });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}

exports.deletePersonal = async id =>{
    try {
        const response = await Personal.findByIdAndUpdate(id, { estado:false } ,{new: true})
        return ({data: response });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}

exports.updateCodePersonal = async (id, rut) => {
    try{
        const response = await Personal.findByIdAndUpdate(id, {codigo: "PER-"+rut + nanoid()} ,{new: true, runValidators:true})
        return ({data: response.codigo });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}
