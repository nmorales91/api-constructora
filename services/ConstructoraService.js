const Constructora = require('../models/constructoraModel')
const mongoose = require('mongoose')

exports.getConstructoras = async () => {
    try {
        const response = await Constructora.find();
        return response
    } catch (e) {
        throw Error('Error al consultar constructoras')
    }
}

exports.saveConstructora = async (rut,nombre,giro,direccion,ciudad, comuna, region, telefono, correo) => {
    const constructora = await Constructora.findOne({rut})
    if(constructora){
        throw Error('Constructora ya existe')
    }

    const response = await Constructora.create({
        rut,nombre,giro,direccion,ciudad, comuna, region, telefono, correo
    })
    return response
}

exports.findConstructora = async id => {
    try{
        const constructora = await Constructora.findById(id)
        if(!constructora){
            return ({data: null,  message: 'Constructora no encontrada' });
        }
        return ({data: constructora,  message: 'Constructora encontrado' });
    } catch (e) {
        if(e instanceof mongoose.Error.CastError){
            return ({data: null,  message: 'Id no válido' });
        }
        throw Error('Error al buscar la constructora')
    }
}

exports.updateConstructora = async (id,constructora,rut,nombre,giro,direccion,ciudad, comuna, region, telefono, correo) => {
    try {
        const newConstructora = {
            rut: rut ? rut : constructora.rut,
            nombre : nombre ? nombre : constructora.nombre,
            giro: giro ? giro : constructora.giro,
            direccion : direccion ? direccion : constructora.direccion,
            ciudad: ciudad ? ciudad: constructora.ciudad,
            comuna : comuna ? comuna : constructora.comuna,
            region : region ? region : constructora.region,
            telefono : telefono ? telefono : constructora.telefono,
            correo : correo ? correo : constructora.correo,
        }
        const response = await Constructora.findByIdAndUpdate(id, { ...newConstructora } ,{new: true, runValidators:true})
        return ({data: response });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}
