const mongoose = require('mongoose');
const Roles = require('../models/rolesModel')

exports.getAllRoles = async () => {
    try {
        const response = await Roles.find();
        return response
    } catch (e) {
        throw Error('Error al consultar Roles')
    }
}

exports.findRol = async id => {
    try{
        const rol = await Roles.findById(id)
        if(!rol){
            return ({data: null,  message: 'Rol no encontrado' });
        }
        return ({data: rol,  message: 'Rol encontrado' });
    } catch (e) {
        if(e instanceof mongoose.Error.CastError){
            return ({data: null,  message: 'Id no válido' });
        }
        throw Error('Error al buscar rol')
    }
}

exports.saveRol = async nombre => {
    nombre = nombre.toUpperCase()
    const rol = await Roles.findOne({nombre})
    if(rol){
        throw Error('Rol ya existe')
    }
    const response = await Roles.create({nombre})
    return response
}