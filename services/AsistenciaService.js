const Asistencia = require('../models/asistenciaModel')
const Personal = require('../models/personalModel')
const Trabajador = require('../models/trabajadorModel')
const mongoose = require('mongoose')
const moment = require('moment')
const momentTimezone = require('moment-timezone')

exports.findUsuario = async (tipo, code) => {
    try{
        let usuario = null;

        if (Number(tipo) === 0){
            console.log("Trabajador Encontrado")
            usuario = await Trabajador.find({codigo:code})
        }else if(Number(tipo) === 1){
            console.log("Personal Encontrado")
            usuario = await Personal.find({codigo:code})
        }

        if(!usuario || usuario.length === 0){
            return ({data: null });
        }
        return ({data: usuario});
    } catch (e) {
        throw Error('Error al consultar usuario')
    }
}

exports.findAsistencia = async id => {
    const today = moment().startOf('day').utc(momentTimezone.tz('America/Santiago').format('YYYY-MM-DDTHH:mm:ss')).toDate()
    const endDay = moment().endOf('day').utc(momentTimezone.tz('America/Santiago').format('YYYY-MM-DDTHH:mm:ss')).toDate()
    const asistencia = await Asistencia.findOne({id_usuario:id,fecha: {
        $gte: today,
        $lte: endDay
    }})
    return asistencia
}

exports.findAsistenciaByDay = async (id, fechaInicio, fechaTermino) => {
    const firstDay = moment(new Date(fechaInicio)).format('YYYY-MM-DD')
    const lastDay = moment(new Date(fechaTermino)).format('YYYY-MM-DD')
    const asistencia = await Asistencia.find({id_usuario:id,fecha: {
        $gte: firstDay,
        $lte: lastDay
    }})
    return asistencia
}

exports.crearAsistencia = async (id, tipo) =>{
    const fecha = moment().utc(momentTimezone.tz('America/Santiago')).format('YYYY-MM-DD')
    const hora = moment().utc(momentTimezone.tz('America/Santiago')).format('HH:mm:ss')
    const asistencia = await Asistencia.create({id_usuario:id,fecha,horas:[{tipo:'Ingreso',hora}],tipo_usuario:tipo})
    return {data:asistencia, message: 'Ingreso registrado'}
}

exports.updateAsistencia = async asistencia => {
    const hora = moment().utc(momentTimezone.tz('America/Santiago')).format('HH:mm:ss')
    let tipo = 'Ingreso'
    let message = 'Ingreso registrado'

    let newAsistencia = {...asistencia._doc}
    let ultimoTipo = newAsistencia.horas[newAsistencia.horas.length-1].tipo

    if(ultimoTipo === 'Ingreso'){
        tipo = 'Salida'
        message = 'Salida registrada'
    }

    const nuevaHora = {tipo, hora}

    newAsistencia.horas.push(nuevaHora)
    const response = await Asistencia.findByIdAndUpdate(asistencia._id, { horas: newAsistencia.horas } ,{new: true})
    return {data:response, message}
}
