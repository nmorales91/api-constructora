const Trabajadores = require('../models/trabajadorModel')
const Contratistas = require('../models/contratistaModel')
const Constructoras = require('../models/constructoraModel')
const mongoose = require('mongoose')
const { nanoid } = require('nanoid')

exports.getAllTrabajadores = async (state=false) => {
    try {
        let trabajadores = []
        if(state){
            trabajadores = await Trabajadores.find({estado:state});
        }else{
            trabajadores = await Trabajadores.find();
        }
        return trabajadores
    } catch (e) {
        throw Error('Error al consultar Trabajadores')
    }
}

exports.saveTrabajador = async (trabajador, contratista, constructora) => {
    const trabajadorFind = await Trabajadores.findOne({rut: trabajador.rut})
    if(trabajadorFind){
        throw Error('Trabajador ya existe')
    }
    const trabajadorSave = {...trabajador, codigo: "TRA-"+trabajador.rut + nanoid()}
    const response = await Trabajadores.create({...trabajadorSave, contratista, constructora})
    return response
}

exports.findTrabajador = async id => {
    try{
        const trabajador = await Trabajadores.findById(id)
        if(!trabajador){
            return ({data: null,  message: 'Trabajador no encontrado' });
        }
        return ({data: trabajador,  message: 'Trabajador encontrado' });
    } catch (e) {
        if(e instanceof mongoose.Error.CastError){
            return ({data: null,  message: 'Id no válido' });
        }
        throw Error('Error al buscar trabajador')
    }
}

exports.updateTrabajador = async (id, trabajadorDB, trabajadorRequest) => {
    try {
        let contratista = trabajadorDB.contratista
        if (trabajadorRequest.id_contratista && trabajadorRequest.id_contratista !== contratista._id) {
            contratista = await Contratistas.findById(trabajadorRequest.id_contratista)
        }
        let constructora = trabajadorDB.constructora
        if (trabajadorRequest.id_constructora && trabajadorRequest.id_constructora !== constructora._id) {
            constructora = await Constructoras.findById(trabajadorRequest.id_constructora)
        }
        let newTrabajador = {...trabajadorDB._doc}

        for (const property in trabajadorRequest) {
            if(property !== 'id_contratista' && property !== 'id_constructora' && trabajadorDB[property] !== trabajadorRequest[property]){
                newTrabajador[property] = trabajadorRequest[property]
            }
        }

        newTrabajador.contratista = contratista
        newTrabajador.constructora = constructora

        const response = await Trabajadores.findByIdAndUpdate(id, { ...newTrabajador } ,{new: true, runValidators:true})
        return ({data: response });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}

exports.deleteTrabajador = async id =>{
    try {
        const response = await Trabajadores.findByIdAndUpdate(id, { estado:false } ,{new: true})
        return ({data: response });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}

exports.updateCodeTrabajador = async (id, rut) => {
    try{
        const response = await Trabajadores.findByIdAndUpdate(id, {codigo: "TRA-"+rut + nanoid()} ,{new: true, runValidators:true})
        return ({data: response.codigo });
    } catch (error) {
        return ({data: null,  message: error.message });
    }
}
