const AsistenciaService = require('../services/AsistenciaService')

exports.saveAsistencia = async (req, res) => {
    const {code} = req.body;
    const codeSubstr = code.substr(0,3)
    let tipo = 0;
    if(codeSubstr === 'PER'){
        tipo = 1;
    }
    const usuario = await AsistenciaService.findUsuario(tipo, code)
    if(!usuario.data) {
        return res.status(400).json({ data: usuario.data, message: "No existe el usuario con código "+code });
    }

    let asistencia = await AsistenciaService.findAsistencia(usuario.data[0]._id)

    if(asistencia){
        if(asistencia.horas.length <= 3){
            asistencia = await AsistenciaService.updateAsistencia(asistencia)
        }else{
            return res.status(400).json({ data:null, message: "Usuario ya registró sus 2 salidas y 2 entradas diarias" });
        }
    }else{
        asistencia = await AsistenciaService.crearAsistencia(usuario.data[0]._id, tipo)
    }

    return res.status(200).json({ asistencia });
}

exports.getAsistencia = async (req, res) => {
    const {tipo, code, fechaInicio, fechaTermino} = req.query;
    const usuario = await AsistenciaService.findUsuario(tipo, code)
    if(!usuario.data) {
        return res.status(400).json({ data: usuario.data, message: "No existe el usuario con código "+code });
    }
    let asistencia = await AsistenciaService.findAsistenciaByDay(usuario.data[0]._id, fechaInicio, fechaTermino)
    if(asistencia.length > 0){
        return res.status(200).json({ data: asistencia, message: "Asistencia de usuario id "+usuario.data[0]._id });
    }
    return res.status(400).json({ data: null, message: "No existe el asistencia para el usuario "+usuario.data[0]._id+" en ese rango de fechas" });

}
