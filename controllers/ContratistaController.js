const ContratistaService = require('../services/ContratistaService')    
const EspecialidadService = require('../services/EspecialidadService')    
const RepresentanteService = require('../services/RepresentanteService')    

exports.getAllContratistas = async (req, res) => {   
    try {
        const response = await ContratistaService.getAllContratistas()
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen contratistas registrados" });
        }
        return res.status(200).send({ data: response, message: "Contratistas obtenidos con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.getActiveContratistas = async (req, res) => {   
    try {
        const response = await ContratistaService.getAllContratistas(true)
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen contratistas registrados" });
        }
        return res.status(200).send({ data: response, message: "Contratistas obtenidos con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.saveContratista = async (req, res) => {  
    let {rut, nombre, id_especialidad, id_representante} = req.body;  
    try {
        if(rut && nombre && id_especialidad && id_representante){

            const especialidad = await EspecialidadService.findEspecialidad(id_especialidad)
            if(!especialidad.data) {
                return res.status(200).json({ data: especialidad.data, message: "No existe la especialidad con id "+id_especialidad });
            }
            const representante = await RepresentanteService.findRepresentante(id_representante)
            if(!representante.data) {
                return res.status(200).json({ data: representante.data, message: "No existe el representante con id "+id_representante });
            }

            const response = await ContratistaService.saveContratista(rut, nombre, especialidad.data, representante.data)
            return res.status(200).json({ data: response, message: "Contratista agregado con éxito" });

        }
        return res.status(400).send({message: 'Ingrese los campos obligatorios'})
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.updateContratista = async (req, res) => {
    const {id} = req.params
    const {rut, nombre, id_especialidad, id_representante} = req.body
    const contratista = await ContratistaService.findContratista(id)

    if(!contratista.data) {
        return res.status(400).json({ data: contratista.data, message: "No existe el contratista con id "+id });
    }

    const response = await ContratistaService.updateContratista(id, contratista.data, rut, nombre, id_especialidad, id_representante)
    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Contratista actualizado con éxito" })

}

exports.findContratista = async (req, res) => {
    const {id} = req.params
    const response = await ContratistaService.findContratista(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe el contratista con id "+id });
    }
    return res.status(200).send({ data: response.data, message: "Contratista obtenido con éxito" })
}

exports.deleteContratista = async (req, res) => {
    const {id} = req.params
    const contratista = await ContratistaService.findContratista(id)
    if(!contratista.data) {
        return res.status(400).json({ data: contratista.data, message: "No existe el contratista con id "+id });
    }
    const response = await ContratistaService.deleteContratista(id)

    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Contratista eliminado con éxito" })
}