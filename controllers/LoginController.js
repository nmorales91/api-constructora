const LoginService = require('../services/LoginService')

exports.loginUser = async (req, res) => {
    let {correo, clave} = req.body;  
    try {
        if(correo && clave) {
            const response = await LoginService.loginUser(correo, clave)
            if(!response.data) {
                return res.status(200).json({ data: null, message: response.message });
            }
            return res.status(200).json({ data: response.data, message: "Usuario logueado con éxito" });
        }
        return res.status(400).send({message: 'Ingrese los campos obligatorios'})
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}