const TrabajadorService = require('../services/TrabajadorService')    
const ContratistaService = require('../services/ContratistaService')    
const ConstructoraService = require('../services/ConstructoraService')    

exports.getAllTrabajadores = async (req, res) => {   
    try {
        const response = await TrabajadorService.getAllTrabajadores()
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen trabajadores registrados" });
        }
        return res.status(200).send({ data: response, message: "Trabajadores obtenidos con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.getActiveTrabajadores = async (req, res) => {   
    try {
        const response = await TrabajadorService.getAllTrabajadores(true)
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen trabajadores registrados" });
        }
        return res.status(200).send({ data: response, message: "Trabajadores obtenidos con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.saveTrabajador = async (req, res) => { 
    const trabajador = req.body;   
    try {
        if(trabajador.rut && trabajador.nombre && trabajador.apellido_paterno && trabajador.id_contratista && trabajador.id_constructora){

            const contratista = await ContratistaService.findContratista(trabajador.id_contratista)
            if(!contratista.data) {
                return res.status(200).json({ data: contratista.data, message: "No existe el contratista con id "+trabajador.id_contratista });
            }
            const constructora = await ConstructoraService.findConstructora(trabajador.id_constructora)
            if(!constructora.data) {
                return res.status(200).json({ data: constructora.data, message: "No existe el constructora con id "+trabajador.id_constructora });
            }
            const response = await TrabajadorService.saveTrabajador(trabajador, contratista.data, constructora.data)
            return res.status(200).json({ data: response, message: "Trabajador agregado con éxito" });

        }
        return res.status(400).send({message: 'Ingrese los campos obligatorios'})
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.updateTrabajador = async (req, res) => {
    const {id} = req.params
    const trabajadorRequest = req.body
    const trabajadorDB = await TrabajadorService.findTrabajador(id)
    if(!trabajadorDB.data) {
        return res.status(400).json({ data: trabajadorDB.data, message: "No existe el trabajador con id "+id });
    }

    const response = await TrabajadorService.updateTrabajador(id, trabajadorDB.data, trabajadorRequest)
    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Trabajador actualizado con éxito" })

}

exports.findTrabajador = async (req, res) => {
    const {id} = req.params
    const response = await TrabajadorService.findTrabajador(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe el trabajador con id "+id });
    }
    return res.status(200).send({ data: response.data, message: "Trabajador obtenido con éxito" })
}

exports.deleteTrabajador = async (req, res) => {
    const {id} = req.params
    const trabajador = await TrabajadorService.findTrabajador(id)
    if(!trabajador.data) {
        return res.status(400).json({ data: trabajador.data, message: "No existe el trabajador con id "+id });
    }
    const response = await TrabajadorService.deleteTrabajador(id)

    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Trabajador eliminado con éxito" })
}

exports.getCodeTrabajador = async (req, res) => {
    const {id} = req.params
    const response = await TrabajadorService.findTrabajador(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe el trabajador con id "+id });
    }
    return res.status(200).send({ data: response.data.codigo, message: "Código obtenido con éxito" })
}

exports.updateCodeTrabajador = async (req, res) => {
    const {id} = req.params
    const trabajador = await TrabajadorService.findTrabajador(id)
    if(!trabajador.data) {
        return res.status(200).json({ data: trabajador.data, message: "No existe el trabajador con id "+id });
    }

    const response = await TrabajadorService.updateCodeTrabajador(id, trabajador.data.rut)
    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Código actualizado con éxito" })
}
