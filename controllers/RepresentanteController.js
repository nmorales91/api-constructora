const RepresentanteService = require('../services/RepresentanteService')    

exports.getRepresentantes = async (req, res) => {   
    try {
        const response = await RepresentanteService.getRepresentantes()
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen representantes registrados" });
        }
        return res.status(200).send({ data: response, message: "Representantes obtenidos con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.saveRepresentantes = async (req, res) => {  
    let {rut, nombre, direccion,ciudad, comuna, region, telefono, correo} = req.body;  
    try {
        if( nombre && rut && correo ){
            const response = await RepresentanteService.saveRepresentantes(rut, nombre, direccion,ciudad, comuna, region, telefono, correo)
            return res.status(200).json({ data: response, message: "Representante agregado con éxito" });
        }
        return res.status(400).send({message: 'Ingrese los campos obligatorios'})
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.findRepresentante = async (req, res) => {
    const {id} = req.params
    const response = await RepresentanteService.findRepresentante(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe el representante con id "+id });
    }
    return res.status(200).send({ data: response.data, message: "Representante obtenido con éxito" })
}
