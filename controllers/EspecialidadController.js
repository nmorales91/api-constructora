const EspecialidadService = require('../services/EspecialidadService')    

exports.getEspecialidades = async (req, res) => {   
    try {
        const response = await EspecialidadService.getEspecialidades()
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen especialidades registradas" });
        }
        return res.status(200).send({ data: response, message: "Especialidades obtenidas con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.saveEspecialidad = async (req, res) => {  
    let {nombre} = req.body;  
    try {
        if( nombre ){
            const response = await EspecialidadService.saveEspecialidad(nombre)
            return res.status(200).json({ data: response, message: "Especialidad agregada con éxito" });
        }
        return res.status(400).send({message: 'Ingrese los campos obligatorios'})
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.findEspecialidad = async (req, res) => {
    const {id} = req.params
    const response = await EspecialidadService.findEspecialidad(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe la especialidad con id "+id });
    }
    return res.status(200).send({ data: response.data, message: "Especialidad obtenida con éxito" })
}
