const UsuarioService = require('../services/UsuarioService')

exports.getAllUsers = async (req, res) => {
    try {
        const response = await UsuarioService.getAllUsers()
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen usuarios registrados" });
        }
        return res.status(200).send({ data: response, message: "Usuarios obtenidos con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.getActiveUsers = async (req, res) => {
    try {
        const response = await UsuarioService.getAllUsers(true)
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen usuarios registrados" });
        }
        return res.status(200).send({ data: response, message: "Usuarios obtenidos con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.saveUser = async (req, res) => {
    let {correo,clave,rol,nombre, apellido_paterno, apellido_materno} = req.body;
    try {
        if(correo && clave && rol && nombre && apellido_paterno){
            const response = await UsuarioService.saveUser(correo,clave,rol,nombre, apellido_paterno, apellido_materno)
            if(response.data){
                return res.status(200).json(response);
            }
            return res.status(400).send({data: response.data, message:response.message})
        }
        return res.status(400).send({message: 'Ingrese los campos obligatorios'})
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.updateUser = async (req, res) => {
    const {id} = req.params
    const {nombre, apellido_paterno, apellido_materno, correo, clave, rol} = req.body
    const usuario = await UsuarioService.findUser(id)

    if(!usuario.data) {
        return res.status(400).json({ data: usuario.data, message: "No existe el usuario con id "+id });
    }

    const response = await UsuarioService.updateUser(id, usuario.data, nombre, apellido_paterno, apellido_materno, correo, clave, rol)

    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Usuario actualizado con éxito" })

}

exports.findUser = async (req, res) => {
    const {id} = req.params
    const response = await UsuarioService.findUser(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe el usuario con id "+id });
    }
    return res.status(200).send({ data: response.data, message: "Usuario obtenido con éxito" })
}

exports.deleteUser = async (req, res) => {
    const {id} = req.params
    const usuario = await UsuarioService.findUser(id)
    if(!usuario.data) {
        return res.status(400).json({ data: usuario.data, message: "No existe el usuario con id "+id });
    }
    const response = await UsuarioService.deleteUser(id)

    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Usuario eliminado con éxito" })
}
