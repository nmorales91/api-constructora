const RolesService = require('../services/RolesService')

exports.getAllRoles = async (req, res) => {   
    try {
        const response = await RolesService.getAllRoles()
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen roles registrados" });
        }
        return res.status(200).send({ data: response, message: "Roles obtenidos con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.findRol = async (req, res) => {
    const {id} = req.params
    const response = await RolesService.findRol(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe el rol con id "+id });
    }
    return res.status(200).send({ data: response.data, message: "Rol obtenido con éxito" })
}

exports.saveRol = async (req, res) => {  
    let {nombre} = req.body;  
    try {
        if(nombre){
            const response = await RolesService.saveRol(nombre)
            return res.status(200).json({ data: response, message: "Rol agregado con éxito" });
        }
        return res.status(400).send({message: 'Ingrese los campos obligatorios'})
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}