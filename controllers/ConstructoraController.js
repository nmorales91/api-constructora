const ConstructoraService = require('../services/ConstructoraService')    

exports.getConstructoras = async (req, res) => {   
    try {
        const response = await ConstructoraService.getConstructoras()
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existen constructoras registradas" });
        }
        return res.status(200).send({ data: response, message: "Constructoras obtenidos con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.saveConstructora = async (req, res) => {  
    let {rut,nombre,giro,direccion,ciudad, comuna, region, telefono, correo} = req.body;  
    try {
        if(correo && rut && nombre ){
            const response = await ConstructoraService.saveConstructora(rut,nombre,giro,direccion,ciudad, comuna, region, telefono, correo)
            return res.status(200).json({ data: response, message: "Constructora agregada con éxito" });
        }
        return res.status(400).send({message: 'Ingrese los campos obligatorios'})
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.updateConstructora = async (req, res) => {
    const {id} = req.params
    const {rut,nombre,giro,direccion,ciudad, comuna, region, telefono, correo} = req.body
    const constructora = await ConstructoraService.findConstructora(id)

    if(!constructora.data) {
        return res.status(400).json({ data: constructora.data, message: "No existe la constructora con id "+id });
    }

    const response = await ConstructoraService.updateConstructora(id, constructora.data, rut,nombre,giro,direccion,ciudad, comuna, region, telefono, correo)

    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Constructora actualizada con éxito" })

}

exports.findConstructora = async (req, res) => {
    const {id} = req.params
    const response = await ConstructoraService.findConstructora(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe la constructora con id "+id });
    }
    return res.status(200).send({ data: response.data, message: "Constructora obtenida con éxito" })
}
