const PersonalService = require('../services/PersonalService')     
const ConstructoraService = require('../services/ConstructoraService')    

exports.getAllPersonal = async (req, res) => {   
    try {
        const response = await PersonalService.getAllPersonal()
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existe personal registrado" });
        }
        return res.status(200).send({ data: response, message: "Personal obtenido con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.getActivePersonal = async (req, res) => {   
    try {
        const response = await PersonalService.getAllPersonal(true)
        if(response.length === 0){
            return res.status(200).json({ data: response, message: "No existe personal registrado" });
        }
        return res.status(200).send({ data: response, message: "Personal obtenido con éxito" })
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.savePersonal = async (req, res) => { 
    const personal = req.body;   
    try {
        if(personal.rut && personal.nombre && personal.apellido_paterno && personal.fecha_ingreso && personal.id_constructora){
            const constructora = await ConstructoraService.findConstructora(personal.id_constructora)
            if(!constructora.data) {
                return res.status(200).json({ data: constructora.data, message: "No existe la constructora con id "+personal.id_constructora });
            }
            const response = await PersonalService.savePersonal(personal, constructora.data)
            return res.status(200).json({ data: response, message: "Personal agregado con éxito" });

        }
        return res.status(400).send({message: 'Ingrese los campos obligatorios'})
    } catch (e) {
        return res.status(400).json({ message: e.message });
    }
}

exports.updatePersonal = async (req, res) => {
    const {id} = req.params
    const personalRequest = req.body
    const personalDB = await PersonalService.findPersonal(id)
    if(!personalDB.data) {
        return res.status(400).json({ data: personalDB.data, message: "No existe el personal con id "+id });
    }

    const response = await PersonalService.updatePersonal(id, personalDB.data, personalRequest)
    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Personal actualizado con éxito" })

}

exports.findPersonal = async (req, res) => {
    const {id} = req.params
    const response = await PersonalService.findPersonal(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe el personal con id "+id });
    }
    return res.status(200).send({ data: response.data, message: "Personal obtenido con éxito" })
}

exports.deletePersonal = async (req, res) => {
    const {id} = req.params
    const personal = await PersonalService.findPersonal(id)
    if(!personal.data) {
        return res.status(400).json({ data: personal.data, message: "No existe el personal con id "+id });
    }
    const response = await PersonalService.deletePersonal(id)

    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Personal eliminado con éxito" })
}

exports.getCodePersonal = async (req, res) => {
    const {id} = req.params
    const response = await PersonalService.findPersonal(id)
    if(!response.data) {
        return res.status(200).json({ data: response.data, message: "No existe el personal con id "+id });
    }
    return res.status(200).send({ data: response.data.codigo, message: "Código obtenido con éxito" })
}

exports.updateCodePersonal = async (req, res) => {
    const {id} = req.params
    const personal = await PersonalService.findPersonal(id)
    if(!personal.data) {
        return res.status(200).json({ data: personal.data, message: "No existe el personal con id "+id });
    }

    const response = await PersonalService.updateCodePersonal(id, personal.data.rut)
    if(!response.data){
        return res.status(400).json({ message: response.message });
    }
    return res.status(200).send({ data: response.data, message: "Código actualizado con éxito" })
}
