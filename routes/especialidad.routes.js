const express = require("express");
const router = express.Router()
const EspecialidadController = require('../controllers/EspecialidadController')

router.get('/',EspecialidadController.getEspecialidades)
router.post('/', EspecialidadController.saveEspecialidad)
router.get('/:id', EspecialidadController.findEspecialidad)

module.exports = router;
