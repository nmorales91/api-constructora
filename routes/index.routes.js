//express
const express = require("express");
const app = express();
const { verificaRol, verificarToken } = require('../middlewares/autenticacion')

app.use("/server/usuarios", require("./usuarios.routes"));
app.use("/server/constructoras",[ verificarToken, verificaRol ], require("./constructora.routes"));
app.use("/server/especialidades",[ verificarToken, verificaRol ], require("./especialidad.routes"));
app.use("/server/representantes", [ verificarToken, verificaRol ] ,require("./representante.routes"));
app.use("/server/contratistas", [ verificarToken, verificaRol ] ,require("./contratista.routes"));
app.use("/server/trabajadores",[ verificarToken, verificaRol ], require("./trabajador.routes"));
app.use("/server/roles", require("./roles.routes"));
app.use("/server/personal",[ verificarToken, verificaRol ], require("./personal.routes"));
app.use("/server/asistencia",[ verificarToken, verificaRol ], require("./asistencia.routes"));
app.use("/server/login", require("./login.routes"));

module.exports = app
