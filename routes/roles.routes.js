const express = require("express");
const router = express.Router();
const RolesController = require("../controllers/RolesController");

router.post("/", RolesController.saveRol);
router.get("/", RolesController.getAllRoles);
router.get("/:id", RolesController.findRol);

module.exports = router;
