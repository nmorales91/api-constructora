const express = require("express");
const router = express.Router();
const ContratistaController = require("../controllers/ContratistaController")

router.get('/',ContratistaController.getActiveContratistas)
router.get('/all', ContratistaController.getAllContratistas)
router.post('/', ContratistaController.saveContratista)
router.put('/:id', ContratistaController.updateContratista)
router.get('/:id', ContratistaController.findContratista)
router.delete('/:id', ContratistaController.deleteContratista)

module.exports = router;
