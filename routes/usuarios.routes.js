const express = require("express");
const router = express.Router();
const UsuarioController = require('../controllers/UsuarioController')
const { verificaRol, verificarToken } = require('../middlewares/autenticacion')


router.get('/', [ verificarToken, verificaRol ] ,UsuarioController.getActiveUsers)
router.get('/all',[ verificarToken, verificaRol ], UsuarioController.getAllUsers)
router.post('/', UsuarioController.saveUser)
router.put('/:id',[ verificarToken, verificaRol ], UsuarioController.updateUser)
router.get('/:id', [ verificarToken, verificaRol ], UsuarioController.findUser)
router.delete('/:id', [ verificarToken, verificaRol ], UsuarioController.deleteUser)

module.exports = router;
