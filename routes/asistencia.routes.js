const express = require("express");
const router = express.Router();
const AsistenciaController = require('../controllers/AsistenciaController')

router.post('/',AsistenciaController.saveAsistencia)
router.get('/',AsistenciaController.getAsistencia)

module.exports = router;
