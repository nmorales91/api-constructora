const express = require("express");
const router = express.Router()
const ConstructoraController = require('../controllers/ConstructoraController')

router.get('/',ConstructoraController.getConstructoras)
router.post('/', ConstructoraController.saveConstructora)
router.put('/:id', ConstructoraController.updateConstructora)
router.get('/:id', ConstructoraController.findConstructora)

module.exports = router;
