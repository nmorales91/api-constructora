const express = require("express");
const router = express.Router();
const TrabajadorController = require('../controllers/TrabajadorController')

router.get('/',TrabajadorController.getActiveTrabajadores)
router.get('/all', TrabajadorController.getAllTrabajadores)
router.post('/', TrabajadorController.saveTrabajador)
router.put('/:id', TrabajadorController.updateTrabajador)
router.get('/:id', TrabajadorController.findTrabajador)
router.delete('/:id', TrabajadorController.deleteTrabajador)
router.get('/code/:id', TrabajadorController.getCodeTrabajador)
router.put('/code/:id', TrabajadorController.updateCodeTrabajador)

module.exports = router;
