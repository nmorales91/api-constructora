const express = require("express");
const router = express.Router();
const PersonalController = require('../controllers/PersonalController')

router.get('/',PersonalController.getActivePersonal)
router.get('/all', PersonalController.getAllPersonal)
router.post('/', PersonalController.savePersonal)
router.put('/:id', PersonalController.updatePersonal)
router.get('/:id', PersonalController.findPersonal)
router.delete('/:id', PersonalController.deletePersonal)
router.get('/code/:id', PersonalController.getCodePersonal)
router.put('/code/:id', PersonalController.updateCodePersonal)

module.exports = router;
