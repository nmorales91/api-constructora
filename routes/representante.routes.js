const express = require("express");
const router = express.Router()
const RepresentanteController = require('../controllers/RepresentanteController')

router.get('/',RepresentanteController.getRepresentantes)
router.post('/', RepresentanteController.saveRepresentantes)
router.get('/:id', RepresentanteController.findRepresentante)

module.exports = router;
